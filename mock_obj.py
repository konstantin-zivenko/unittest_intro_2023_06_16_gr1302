from unittest.mock import Mock
from datetime import datetime


def is_weekday():
    today = datetime.today()
    return 0 <= today.weekday() < 5


if __name__ == "__main__":
    friday = datetime(year=2023, month=6, day=16)
    sunday = datetime(year=2023, month=6, day=18)
    datetime = Mock()
    datetime.today.return_value = friday
    assert is_weekday()
    datetime.today.return_value = sunday
    assert not is_weekday()
